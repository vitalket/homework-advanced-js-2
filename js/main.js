/* 
   1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
    - Коли можуть виникнути помилки в коді через нашу неуважність, непередбачувані вхідні дані від користувачів, неправильні відповіді від сервера або з тисяч інших причин. Конструкція try...catch дозволяє нам перехоплювати помилки, що дає змогу скриптам виконати потрібні дії, а не раптово припинити роботу скрипта.
 */


const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

const rootDiv = document.createElement('div');
rootDiv.id = 'root';
document.body.appendChild(rootDiv);

const list = document.createElement('ul');

books.forEach((book, index) => {
  try {
    if (!book.author) {
      throw new Error(`Відсутня властивість 'author' у книзі ${index} '${book.name}'`);
    }
    if (!book.name) {
      throw new Error(`Відсутня властивість 'name' у книзі ${index} '${book.name}'`);
    }
    if (!book.price) {
      throw new Error(`Відсутня властивість 'price' у книзі ${index} '${book.name}'`);
    }

    const liElem = document.createElement('li');
    liElem.textContent = `Автор: ${book.author}. Назва: ${book.name}. Ціна: ${book.price}`;
    list.appendChild(liElem);
    
  } catch(e) {
    console.log(e.message);
  }
    
  })

rootDiv.appendChild(list);

















